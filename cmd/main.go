package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"

	"golang.org/x/mod/modfile"
	"golang.org/x/mod/module"
	"golang.org/x/mod/zip"
)

func main() {
	ver := "v0.0.0-20170922011244-0744d001aa84"
	projectDir := "../test-gomodule"

	projectPath, err := getModulePath(projectDir)
	if err != nil {
		fmt.Printf("error: extracting module path: %v\n", err)
		return
	}

	file, err := packCodebase(
		projectPath,
		ver,
		projectDir,
	)
	if err != nil {
		fmt.Printf("error: %v\n", err)
		return
	}

	_, err = createInfoFile(ver)
	if err != nil {
		fmt.Printf("error: falied to creete info file: %v\n", err)
		return
	}

	err = createModFile("../test-gomodule/go.mod", ver+".mod")
	if err != nil {
		fmt.Printf("error: falied to create mod file: %v\n", err)
		return
	}

	err = populateCache(projectPath)
	if err != nil {
		fmt.Printf("error: failed to populate cache: %v\n", err)
		return
	}

	fmt.Printf("generated file: %s\n", file)
}

func packCodebase(repo, version, directory string) (string, error) {
	output := fmt.Sprintf("%s.zip", version)
	f, err := os.Create(output)
	if err != nil {
		return "", fmt.Errorf("failed to initilise output file: %v", err)
	}
	defer f.Close()
	ver := module.Version{
		Path:    repo,
		Version: version,
	}

	return output, zip.CreateFromDir(
		f,
		ver,
		directory,
	)
}

func createInfoFile(version string) (string, error) {
	info := map[string]string{
		"Version": version,
	}
	fileName := fmt.Sprintf("%s.info", version)
	f, err := os.Create(fileName)
	if err != nil {
		return "", fmt.Errorf("failed to initilise output file: %v", err)
	}

	return fileName, json.NewEncoder(f).Encode(info)
}

func createModFile(modFilePath, dest string) error {
	destFile, err := os.Create(dest)
	if err != nil {
		return fmt.Errorf("failed to create destination file: %v", err)
	}
	defer destFile.Close()

	modFile, err := os.Open(modFilePath)
	if err != nil {
		return fmt.Errorf("failed to open mod file: %v", err)
	}
	defer modFile.Close()

	_, err = io.Copy(destFile, modFile)
	return err
}

func getModulePath(dir string) (string, error) {
	filePath := fmt.Sprintf("%s/go.mod", dir)
	modFile, err := os.Open(filePath)
	if err != nil {
		return "", fmt.Errorf("failed to open mod file: %v", err)
	}
	defer modFile.Close()

	content, err := io.ReadAll(modFile)
	if err != nil {
		return "", fmt.Errorf("failed read mod file: %v", err)
	}

	modulePath := modfile.ModulePath(content)
	if len(modulePath) == 0 {
		return "", fmt.Errorf("ivalid module path '%s'", modulePath)
	}

	return modulePath, nil
}

func populateCache(modulePath string) error {
	gopath, ok := os.LookupEnv("GOPATH")
	if !ok {
		return errors.New("GOPATH env variable not set")
	}

	cacheDir := fmt.Sprintf("%s/pkg/mod/cache/download/%v/@v/", gopath, modulePath)
	err := os.MkdirAll(cacheDir, 0755)
	if err != nil {
		return err
	}

	return nil
}
